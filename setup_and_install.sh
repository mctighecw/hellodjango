#!/bin/bash

echo Setting up Python virtualenv and installing dependencies...

virtualenv -p python3 venv
source venv/bin/activate
python --version

pip install -r requirements.txt
