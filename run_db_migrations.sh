#!/usr/bin/env bash

echo Running Django database migrations...

# If migrations have not been run
# python manage.py makemigrations citydatabase
# python manage.py sqlmigrate citydatabase 0001

python manage.py migrate
