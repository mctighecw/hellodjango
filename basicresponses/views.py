from django.shortcuts import render
from django.http import HttpResponse


def home(request):
    return HttpResponse("Hello Django!")

def about(request):
    return HttpResponse("This is a playground app, to learn about Django.")

def info(request):
    return HttpResponse("This was created by Christian McTighe in Summer 2018.")
