from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home),
    url(r'^about/$', views.about),
    url(r'^info/$', views.info),
]
