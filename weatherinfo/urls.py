from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^city/(?P<city_name>\w{1,20})/$', views.get_city),
]
