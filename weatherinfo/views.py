from django.shortcuts import render
from django.http import HttpResponse

from weather import Weather, Unit


def get_city(request, city_name):
    print(city_name)

    weather = Weather(unit=Unit.CELSIUS)
    location = weather.lookup_by_location(city_name)
    condition = location.condition
    text = condition.text
    response = "The weather forecast for " + city_name.capitalize()  + ": " + text

    return HttpResponse(response)
