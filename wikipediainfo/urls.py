from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^summary/(?P<subject>\w{1,20})/$', views.get_summary),
    url(r'^page/(?P<subject>\w{1,20})/$', views.get_page),
]
