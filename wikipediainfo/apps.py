from django.apps import AppConfig


class WikipediainfoConfig(AppConfig):
    name = 'wikipediainfo'
