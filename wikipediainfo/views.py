from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

import wikipedia


def get_summary(request, subject):
    print(subject)
    response = wikipedia.summary(subject)

    return HttpResponse(response)


def get_page(request, subject):
    print(subject)
    data = wikipedia.page(subject)
    response = { "title": data.title, "content": data.content, "url": data.url }

    return JsonResponse(response)
