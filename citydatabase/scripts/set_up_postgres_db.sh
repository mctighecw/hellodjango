#!/usr/bin/env bash

# Set up postgres database
echo Setting up hellodjango_database...
echo Current user: $USER

# Comment these out if already created
echo Creating user: hellodjango_user. Please enter password 'password'
sudo -u $USER createuser -P hellodjango_user

echo Closing any open db connections...
sudo -u $USER psql -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = 'hellodjango_database' AND pid <> pg_backend_pid();"

echo Deleting old db...
sudo -u $USER dropdb hellodjango_database

echo Creating new db...
sudo -u $USER createdb -O hellodjango_user -E 'utf8' -T 'template0' hellodjango_database
