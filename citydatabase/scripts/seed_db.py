from citydatabase.models import City

# Seed database with cities data

def add_db_cities():
    c1 = City(city='London', country='United Kingdom', continent='Europe', language='English')
    c1.save()

    c2 = City(city='Tokyo', country='Japan', continent='Asia', language='Japanese')
    c2.save()

    c3 = City(city='New York', country='United States', continent='North Amercia', language='English')
    c3.save()

    c4 = City(city='Paris', country='France', continent='Europe', language='French')
    c4.save()

    c5 = City(city='Sydney', country='Australia', continent='Australia', language='English')
    c5.save()
