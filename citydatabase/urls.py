from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^all/', views.get_all),
    url(r'^single/(?P<city_name>.+?)/$', views.get_city_info),
]
