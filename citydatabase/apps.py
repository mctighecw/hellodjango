from django.apps import AppConfig


class CitydatabaseConfig(AppConfig):
    name = 'citydatabase'
