from django.db import models

class City(models.Model):
    """
    City model

    Fields: city, country, continent, language
    """

    # id field added automatically and auto-incremented
    city = models.CharField(max_length=50)
    country = models.CharField(max_length=50)
    continent = models.CharField(max_length=50)
    language = models.CharField(max_length=50)


    def __str__(self):
        return self.city

    def get_full_location(self):
        return ", ".join((self.city, self.country))

    def to_json(self):
        city_dict = dict(city=self.city, country=self.country, continent=self.continent, language=self.language)
        return city_dict
