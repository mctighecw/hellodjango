from django.http import JsonResponse

from citydatabase.models import City


# get all cities
def get_all(request):
    query_set = City.objects.all()
    count = query_set.count()

    if query_set.exists():
        results = dict()
        for item in query_set.iterator():
            item_json = item.to_json()
            results.update({ item.id: item_json })

        result = { "status": "success", "count": count, "results": results }

    else:
        result = { "status": "fail", "message": "No cities found" }

    return JsonResponse(result)


# get city by name
def get_city_info(request, city_name):
    capitalized_city = city_name.capitalize()

    query_result = City.objects.filter(city=capitalized_city).first()

    if query_result is not None:
        query_json = query_result.to_json()
        result = { "status": "success", "result": query_json }
    else:
        result = { "status": "fail", "message": "City not found" }

    return JsonResponse(result)
