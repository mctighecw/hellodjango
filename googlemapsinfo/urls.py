from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^geocode/(?P<address>.+?)/$', views.geocode_address),
    url(r'^reversegeocode/(?P<lat>-?\d+\.\d+),(?P<lng>-?\d+\.\d+)/$', views.reverse_geocode),
    url(r'^directions/(?P<start>.+?),(?P<end>.+?)/$', views.walking_directions),
]
