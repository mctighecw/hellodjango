from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

import googlemaps
from hellodjango.settings import env

"""
Google Maps API Services available:

Geocoding API
Directions API
Distance Matrix API
Elevation API
Geolocation API
Places API
Roads API
Time Zone API
"""

GOOGLE_API_KEY = env('GOOGLE_API_KEY')

gmaps = googlemaps.Client(key=GOOGLE_API_KEY)

def geocode_address(request, address):
    """
    Geocoding an address

    Format: '1600 Amphitheatre Parkway, Mountain View, CA'
    """

    geocode_result = gmaps.geocode(address)
    return HttpResponse(geocode_result)


def reverse_geocode(request, lat, lng):
    """
    Look up an address with reverse geocoding

    Format: '40.714224,-73.961452'
    """

    lat = float(lat)
    lng = float(lng)

    reverse_geocode_result = gmaps.reverse_geocode((lat,lng))
    return HttpResponse(reverse_geocode_result)


def walking_directions(request, start, end):
    """
    Get walking directions from start location to end location

    Format: 'Sydney Town Hall','Parramatta, NSW'
    """

    directions_result = gmaps.directions(start, end, mode="walking")
    return HttpResponse(directions_result)
