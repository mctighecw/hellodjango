from django.apps import AppConfig


class GooglemapsinfoConfig(AppConfig):
    name = 'googlemapsinfo'
