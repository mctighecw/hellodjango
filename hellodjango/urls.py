"""
hellodjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView

# Seed db after it has been created and migrations have been run
# from citydatabase.scripts.seed_db import add_db_cities
# add_db_cities()

urlpatterns = [
    url(r'^', include('basicresponses.urls')),
    url(r'^cities/', include('citydatabase.urls')),
    url(r'^maps/', include('googlemapsinfo.urls')),
    url(r'^weather/', include('weatherinfo.urls')),
    url(r'^wikipedia/', include('wikipediainfo.urls')),
    url(r'^favicon.ico$', RedirectView.as_view(url=staticfiles_storage.url('favicon.png'), permanent=False), name="favicon"),
    path('admin/', admin.site.urls),
]
