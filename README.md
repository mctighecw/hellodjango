# README

My first project with the Python framework Django.

It contains a number of individual apps, including a weather API and a Google Maps API.

## App Information

App Name: hellodjango

Created: July 2018

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/hellodjango)

## Setup

1. Create .env file in root directory and add:
  * ENVIRONMENT (development or production)
  * SECRET_KEY (if available)
  * GOOGLE_API_KEY

2. Set up Python venv and install requirements

    ```
    source setup_and_install.sh
    ```

3. Set up Postgres database and run migrations

    a. Run scripts

    ```
    source citydatabase/scripts/set_up_postgres_db.sh
    source run_db_migrations.sh
    ```

    b. Uncomment lines 25-26 in `hellodjango/hellodjango/urls.py` and start server

    ```
    source run.sh
    ```

4. Make Django admin

    ```
    python manage.py createsuperuser
    admin
    admin@example.com
    django1234
    ```

Last updated: 2025-03-05
